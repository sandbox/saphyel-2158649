<?php

/**
 * Plugin implementation of the 'BnetURL' widget.
 *
 * @BnetURL(
 *   id = "BnetURL",
 * )
 */
namespace Drupal\blizzard\Controller;

use Doctrine\Common\Proxy\Exception\InvalidArgumentException;
use Drupal\blizzard\Model\StatisticsStorage;

abstract class BnetURL {
  
  const URL_HOST = '://{region}.battle.net';

  protected $apiPath = '/api';
  protected $apiType = 'Unknown';
  protected $data;
  protected $fields = array();
  protected $placeholders = array();
  protected $region;
  protected $replacements = array();
  protected $url = '';
  

  public function __construct($region = 'eu') {
    $this->setRegion($region);
  }

  protected function addPlaceholder($placeholder, $replacement) {
    $i = array_search($placeholder, $this->placeholders);
    if ($i === FALSE) {
      $this->placeholders[] = $placeholder;
      $this->replacements[] = rawurlencode($replacement);
    }
    else {
      $this->replacements[$i] = rawurlencode($replacement);
    }
  }

  protected function setApiType($api_type) {
    $this->apiType = $api_type;
  }

  protected function setApiPath($api_path, $placeholders = array()) {
    $this->apiPath = $api_path;
    
    if (!empty($placeholders)) {
      foreach ($placeholders as $placeholder => $replacement) {
        $this->addPlaceholder($placeholder, $replacement);
      }
    }
  }
  
  protected static function decodeJson($data, $encoding = '') {
    switch ($encoding) {
      case 'gzip':
        $json = gzinflate(substr($data, 10, -8));
        break;
      default:
        $json = $data;
    }
    
    $json = json_decode($json, TRUE);
    
    return $json;
  }
  
  public function sendRequest() {
    $this->addPlaceholder('{region}', $this->region);
    $ssh = self::checkAuthentication() ? 'https' : 'http';
    $this->url = $ssh . self::URL_HOST . $this->apiPath;
    $this->url = str_replace($this->placeholders, $this->replacements, $this->url);
    
    if (extension_loaded('zlib')) {
      $headers['Accept-Encoding'] = 'gzip';
    }
    
    $response =  \Drupal::service('http_default_client')
      ->get($this->url, $headers)
      ->send()
      ->getBody(TRUE);
    $encoding = isset($response->headers['content-encoding']) ? $response->headers['content-encoding'] : '';
    $this->data = self::decodeJson($response, $encoding);
    
    $this->setFields($this->data);
    StatisticsStorage::updateAPI($this->apiPath);
    return $this;
  }
  
  public function setRegion($region = 'eu') {
    $this->region = ($region == 'us') ? $region : 'eu';
  }
  
  public function setFields($fields) {
    $this->fields = $fields;
  }

  public function getFields() {
    return $this->fields;
  }
  
  final public static function checkAuthentication() {
    $public_key = variable_get('blizzard_public_key', '');
    $private_key = variable_get('blizzard_private_key', '');
    
    if (empty($public_key) || empty($private_key) || !extension_loaded('openssl')) {
      return FALSE;
    }
    
    return TRUE;
  }
  
  final public static function generateAuthenticationHeaders($url, $method = 'GET') {
    $public_key = variable_get('blizzard_public_key', '');
    $private_key = variable_get('blizzard_private_key', '');
    $date = gmdate('D, d M Y H:i:s T', REQUEST_TIME);
    $headers['Date'] = $date;
    
    if (!self::checkAuthentication()) {
      return $headers;
    }
    
    // Get the URL path and decode certain characters that cause invalid signatures.
    $path = parse_url($url, PHP_URL_PATH);
    $path = str_replace(array('%27', '%28', '%29'), array('\'', '(', ')'), $path);
    
    // Merge request details into a string to sign.
    $data = "$method\n$date\n$path\n";
    
    // Sign the data string using the private key.
    $signature = base64_encode(hash_hmac('sha1', $data, $private_key, TRUE));
    
    $headers['Authorization'] = "BNET $public_key:$signature";
    return $headers;
  }
  
  protected static function updateCounter($api) {
    
    $query = db_merge('blizzard');
    $fields = array(
      'total' => 1,
      'perday' => 1,
      'timestamp' => REQUEST_TIME
    );
    
    if ($cache) {
      $query->insertFields(array('total_cached' => 1, 'perday_cached' => 1))
        ->expression('total_cached', 'total_cached + 1')
        ->expression('perday_cached', 'perday_cached + 1');
    }
    
    $query->key(array('api' => $counter_id))
      ->fields($fields)
      ->expression('total', 'total + 1')
      ->expression('perday', 'perday + 1')
      ->execute();
  }
}


class BnetURLException extends \Exception {
  public function __construct($message = '', $code = 0, $previous = NULL) {
    // Since PHP 5.3.0
    if (method_exists($this, 'getPrevious')) {
      parent::__construct($message, $code, $previous);
    }
    else {
      parent::__construct($message, $code);
    }
  }
}

class BnetURLJsonException extends BnetURLException {
  public function __construct($code = JSON_ERROR_NONE, $previous = NULL) {
    $message = BnetURLJsonException::getJsonErrorMessage($code);
    parent::__construct($message, $code, $previous);
  }
  
  protected static function getJsonErrorMessage($code) {
    switch ($code) {
      case JSON_ERROR_CTRL_CHAR:
        return 'Control character error, possible incorrect encoding';
      case JSON_ERROR_DEPTH:
        return 'The maximum stack depth has been exceeded';
      case JSON_ERROR_NONE:
        return 'No error has occured';
      case JSON_ERROR_STATE_MISMATCH:
        return 'Invalid or malformed JSON';
      case JSON_ERROR_SYNTAX:
        return 'Syntax error';
      default:
        // Since PHP 5.3.3
        if (defined(JSON_ERROR_UTF8) && $code == JSON_ERROR_UTF8) {
          return 'Malformed UTF-8 characters, possible incorrect encoding';
        }
        return 'Unable to decode JSON string';
    }
  }
}
