<?php

/**
 * @file
 * Contains Drupal\blizzard\Model\StatisticsStorage.
 */

namespace Drupal\blizzard\Model;

class StatisticsStorage {
  
  static function updateAPI($api) {
    $date = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
    $query = db_select('blizzard', 'b')
      ->fields('b', array('bid', 'api', 'daily', 'timestamp'))
      ->condition('b.api', $api, 'like')
      ->condition('b.timestamp', $date, '=')
      ->execute()
      ->fetchAssoc();
    //drupal_set_message($query);
    if ($query) {
      $id=$query['bid'];
      $daily=$query['daily']+1;
      //TODO Update
      db_update('blizzard')
        ->fields(array('daily' => $daily,))
        ->condition('bid', $id, '=')
        ->execute();
    }
    else {
      db_insert('blizzard')
        ->fields(array(
          'api' => $api,
          'daily' => 1,
          'timestamp' => $date,
        ))
        ->execute();
    }
  }
  static function getAll($order, $sort, $filter = NULL) {
    $query = db_select('blizzard', 'b')
      ->fields('b', array('api', 'daily', 'timestamp',))
      ->orderBy($order, $sort)
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->limit(25)
      ->execute();
    return $query;
  }
  
  static function getTotals($filter = NULL) {
    $query = db_select('blizzard', 'b')
      ->fields('b', array('timestamp', 'daily'))
      ->range(0, 1)
    //$query->addExpression('SUM(b.daily)', 'total_counter');
      //->groupBy('b.timestamp')
      ->execute()->fetchField();
    return $query;
  }
}