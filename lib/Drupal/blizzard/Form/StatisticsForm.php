<?php

/**
 * @file
 * Contains Drupal\blizzard\Form\StatisticsForm.
 */

namespace Drupal\blizzard\Form;

use Drupal\Core\Form\FormBase;
use Drupal\blizzard\Model\StatisticsStorage;

/**
 * Configure services form.
 */
class StatisticsForm extends FormBase {

  /**
   * Form ID
   */
  public function getFormId() {
    return 'statistics_report_form';
  }

  /**
   * Build services form.
   */
  public function buildForm(array $form, array &$form_state) {
    
    if (isset($_GET['sort'])) {
      $sort = ($_GET['sort'] == 'asc') ? strtoupper($_GET['sort']) : 'DESC';
    }
    else {
      $sort = 'ASC';
    }
    if (isset($_GET['order'])) {    
      switch($_GET['order']){
        case 'API':
          $order = 'api';
          break;
        case 'Requests':
          $order = 'daily';
          break;
        default:
          $order = 'timestamp';
      }
    }
    else {
      $order = 'timestamp';
    }
    $sumAll = 0;
    $sum = 0;
    $todayIs = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
    foreach (StatisticsStorage::getAll($order, $sort) as $row) {
      $sumAll += $row->daily;
      $sum += ($todayIs == $row->timestamp) ? $row->daily : 0;
      $rows[] = array(
        t($row->api),
        $row->daily,
        date('Y-m-d',$row->timestamp),
      );
    }
    if (!empty($rows)) {
      $rows[] = array(
        'data' => array(
          array('header' => TRUE, 'data' => t('Total: Today | All')),
          $sum,
          $sumAll,
        ),
      );
    }

    $render_array2['blizzard_filters'] = array(
      '#type' => 'fieldset',
      '#title' => t('Filter option'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    
    $render_array['blizzard_table'] = array(
      '#theme' => 'table',
      '#header' => array(
        array('data' => t('API'), 'field' => 'api'),
        array('data' => t('Requests'), 'field' => 'daily'),
        array('data' => t('Date'), 'field' => 'timestamp', 'sort' => 'asc'),
      ),
      '#rows' => $rows,
      '#empty' =>t('Your log is empty'),
    );
    
    return $render_array;
  }
  
  /**
   * Submit form.
   */
  public function submitForm(array &$form, array &$form_state) {
    
  }
  
}
  