<?php

/**
 * @file
 * Contains Drupal\blizzard\Form\BnetServicesForm.
 */

namespace Drupal\blizzard\Form;

use Drupal\Core\Form\FormBase;

/**
 * Configure services form.
 */
class BnetServicesForm extends FormBase {

  /**
   * Form ID
   */
  public function getFormId() {
    return 'bnet_services_form';
  }

  /**
   * Build services form.
   */
  public function buildForm(array $form, array &$form_state) {
    $openssl = extension_loaded('openssl');
    
    $form['authentication']['bnet_list'] = array(
      '#theme' => 'item_list',
      '#title' => t('You will be able to access it:'),
      '#items' => array(
        t('10,000 unauthenticated requests per day.'),
        t('50,000 authenticated requests per day.'),
        t('Not use multiple forms of access, including making any combination of unauthenticated and authenticated requests or using multiple API keys, to make more requests than permitted.'),
        t('Not use other third-party services to make additional requests on their behalf.'),
        t('Not sell, share, transfer, or distribute application access keys or tokens.'),
      ),
    );
    
    if (!$openssl) {
      $form['authentication']['blizzard_list']['#description'] = '<strong class="error">Your PHP installation does not have SSL support enabled. Authentication has been disabled.</strong>';
    }
    
    $form['authentication']['blizzard_public_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Public key'),
      '#default_value' => variable_get('blizzard_public_key', ''),
      '#disabled' => !$openssl
  );
  
  $form['authentication']['blizzard_private_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Private key'),
    '#default_value' => variable_get('blizzard_private_key', ''),
    '#disabled' => !$openssl
  );

    return $form;
  }
  
  /**
   * Validates the submitted form.
   */
  public function validateForm(array &$form, array &$form_state) {
    $values = $form_state['values'];
  
    if ((empty($values['blizzard_public_key']) && !empty($values['blizzard_private_key'])) || (!empty($values['blizzard_public_key']) && empty($values['blizzard_private_key']))) {
      form_set_error('authentication', t('Authentication is not possible without both keys.'));
    }
  }
  
  /**
   * Submit form.
   */
  public function submitForm(array &$form, array &$form_state) {
    
  }
  
}